#include <stdio.h>

int main() {
  int input;
  printf("Type a number! ");
  scanf("%d", &input);
  if ((input/2)*2 == input)
    printf("Your number is even!\n");
  else
    printf("Your number is odd!\n");
}
