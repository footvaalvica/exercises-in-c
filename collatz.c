#include <stdio.h>

int main()
{
  long long int n;
  printf("What number do you want to pass trough the Collatz conjecture? ");
  scanf("%lld", &n);
  while (n != 1) {
    if ((n/2)*2 == n) {
      n = n/2;
      printf("%lld\n", n);
    }
    else {
      n = (n*3)+1;
      printf("%lld\n", n);
    }
  }
}
