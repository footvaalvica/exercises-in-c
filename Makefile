all:
	make palavras
	make ex9
	make hello-world
	make collatz
	make numeo
	make fizzbuzz

clean:
	rm -rf palavras
	rm -rf ex9
	rm -rf hello-world
	rm -rf collatz
	rm -rf numeo
	rm -rf fizzbuzz
