#include <stdio.h>

int main() {
  long long int num;
  printf("Do you want to fizzbuzz up to what number? ");
  scanf("%lld", &num);
  for (int i = 1; i <= num; i++) {
    if (i % 15 == 0) {
      printf("Fizz Buzz \n");
    }
    else if (i % 5 == 0) {
      printf("Buzz \n");
    }
    else if (i % 3 == 0) {
      printf("Fizz \n");
    }
    else {
      printf("%d \n", i);
    }
  }
}
